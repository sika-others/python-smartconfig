#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name = "smartconfig",
    version = "1",
    url = 'https://github.com/ondrejsika/python-smartconfig/',
    license = 'MIT',
    description = "Eas(ier) way to create Python config files",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    requires = [],
    include_package_data = False,
    py_modules = [
        "smartconfig",
    ],
)
